#importing os module
import os

def create_directory(dirName):
  path = "C:/Users/sseera/Desktop/Python/Workspace/{0}".format(dirName)
  if not os.path.exists(dirName):
    os.mkdir(dirName)
    print("Directory ", dirName ," Created ")
  else:    
    print("Directory ", dirName ," already exists")
  
#Calling with specified parameters
create_directory(dirName="Naveen")


#Function to create a file
def create_file(dirName,name,ext):
  #Default path which accepts variable as user specified name and extension
  path = "C:/Users/sseera/Desktop/Python/Workspace/{0}/{1}{2}".format(dirName,name,ext)
  #Opens the File with specified path
  #'w' – open a file for writing. If the file doesn’t exist it creates a new one 
  # otherwise, it’ll overwrite the contents of the existing file.
  f=open(path, 'w')
  #Description to write inside a file
  f.write('Created a new text file!')
  return "Created a new file in {0}".format(path)


#Calling with specified parameters
print(create_file("Naveen","sid",".txt"))
